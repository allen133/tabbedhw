﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TabbedViewHW
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TabbedPage1 : TabbedPage
    {
        int orderTotal = 0;
        public TabbedPage1()
        {
            InitializeComponent();
        }

        async void OnCoffee(object sender, EventArgs e)
        {
            orderTotal = orderTotal + 2;
            OrderText.Text = orderTotal.ToString();
            await DisplayAlert("Order Placed!", "Your Coffee Order has Been Added!", "OK");
        }

        async void OnTea(object sender, EventArgs e)
        {
            orderTotal = orderTotal + 2;
            OrderText.Text = orderTotal.ToString();
            await DisplayAlert("Order Placed!", "Your Tea Order has Been Added!", "OK");
        }

        async void OnWater(object sender, EventArgs e)
        {
            orderTotal = orderTotal + 1;
            OrderText.Text = orderTotal.ToString();
            await DisplayAlert("Order Placed!", "Your Water Order has Been Added!", "OK");
        }

        async void OnSandwich(object sender, EventArgs e)
        {
            orderTotal = orderTotal + 5;
            OrderText.Text = orderTotal.ToString();
            await DisplayAlert("Order Placed!", "Your Sandwich Order has Been Added!", "OK");
        }

        async void OnPlace(object sender, EventArgs e)
        {
            if(orderTotal == 0)
            {
                await DisplayAlert("No Charge", "No Items Have Been Ordered", "OK");
            }
            else
            {
                
                await DisplayAlert("Order Charged", "Your Order has Been Charged", "OK");
            }
           
        }

        async void OnClear(object sender, EventArgs e)
        {
            orderTotal = 0;
            OrderText.Text = orderTotal.ToString();
            await DisplayAlert("Order Cleared", "Your Order Has Been Cleared", "OK");
        }

    }
}